Python Sudoku Translations HowTo


Add a new translation:
To add a new translation follow these instructions:
- Change directory to po/
- Execute "l10n --update" to get a updated version of pythonsudoku.pot.
- Copy pythonsudoku.pot to LANGUAGE.po (for example gl.po).
- Translate the strings.
- Set the charset to UTF-8 and save the file as UTF-8 (iconv(1) for help).
- Execute "l10n --install" to convert the translation to GNU gettext format
  and move it to the correct directory.

Update a translation:
To update a already created translation, due to new translatable strings added
to sources or deleted strings, follow these instructions:
- Change directory to po/
- Execute "l10n --update".
- Execute "l10n --check" to see the changed strings.
- If there are changes, open LANGUAGE.po (for example gl.po), change the
  strings and remove the "#, fuzzy" lines.
- Set the charset to UTF-8 and save the file as UTF-8 (iconv(1) for help).
- Execute "l10n --install" to convert the translation to GNU gettext format
  and move it to the correct directory.
